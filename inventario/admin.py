from django.contrib import admin
from .models.user import User
from .models.product import Product
from .models.category import Category

# Register your models here.# 3 modelos en total


class AccountAdmin(admin.ModelAdmin):
    list_per_page = 20
    list_display = ('id', 'name', 'last_name', 'DNI_type', 'DNI_number','birthdate','avatar' )
    search_fields = ['DNI_number',]
    list_filter = ('DNI_type',)

class ProductAdmin(admin.ModelAdmin):
    list_per_page = 20
    list_display = ('id_product', 'product_name', 'product_description', 'price', 'id_creator','units_in_stock','url_imagen','product_image' )
    search_fields = ['price',]
    list_filter = ('price',)

class CategoryAdmin(admin.ModelAdmin):
    list_per_page = 20
    list_display = ('id_category', 'category_name', 'category_description', 'is_active' )
    search_fields = ['category_name',]
    list_filter = ('is_active',)

admin.site.register(User,AccountAdmin)
admin.site.register(Product,ProductAdmin)
admin.site.register(Category,CategoryAdmin)