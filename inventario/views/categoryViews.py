from rest_framework import views, status
from rest_framework.response import Response
from inventario.models import Category
from inventario.serializers import CategorySerializer
from rest_framework import generics

# List all accounts and Create a account
class CategoryListCreateView(generics.ListCreateAPIView):
    queryset = Category.objects.all()
    serializer_class = CategorySerializer

# Retrieve, Update, Delete a account
class CategoryRetrieveUpdateDeleteView(generics.RetrieveUpdateDestroyAPIView):
    queryset = Category.objects.all()
    serializer_class = CategorySerializer