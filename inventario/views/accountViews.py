from rest_framework import views, status
from rest_framework.response import Response
from rest_framework.authtoken.models import Token
from inventario.models import User
from inventario.serializers import AccountSerializer
from rest_framework import generics
from django.contrib.auth.models import User as Usuario2


# List all accounts and Create a account
class AccountListCreateView(generics.ListCreateAPIView):
    queryset = User.objects.all()
    serializer_class = AccountSerializer

# Retrieve, Update, Delete a account
class AccountRetrieveUpdateDeleteView(generics.RetrieveUpdateDestroyAPIView):
    queryset = User.objects.all()
    serializer_class = AccountSerializer

class InfoView(views.APIView):
    def get(self, request):
        #Id = User.objects.get(username = "angelg")
        Id = Token.objects.get(key = request.headers["Authorization"][6:])
        code = status.HTTP_200_OK
        data = {"id" : Id.user_id}
        #print(request.headers["Authorization"][6:])
        #print("hhhh")
        return Response(data, status = code)

class ProfileView(views.APIView):
    def get(self, request):
        Id = Token.objects.get(key = request.headers["Authorization"][6:])
        code = status.HTTP_200_OK
        user = User.objects.get(id_user_default_id = Id.user_id)
        #print(user.id)
        #data = user
        data = {"id" : user.id}
        return Response(data, status = code)






