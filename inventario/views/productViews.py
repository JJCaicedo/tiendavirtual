from rest_framework import views, status
from rest_framework.response import Response
from inventario.models import Product
from inventario.serializers import ProductSerializer
from rest_framework import generics

# List all accounts and Create a account
class ProductListCreateView(generics.ListCreateAPIView):
    queryset = Product.objects.all()
    serializer_class = ProductSerializer

# Retrieve, Update, Delete a account
class ProductRetrieveUpdateDeleteView(generics.RetrieveUpdateDestroyAPIView):
    queryset = Product.objects.all()
    serializer_class = ProductSerializer