from django.db import models
from django.contrib.auth.models import User

class User(models.Model):
    id = models.AutoField(primary_key=True) 
    name = models.CharField(max_length=30)
    last_name= models.CharField(max_length=30)
    DNI_type= models.CharField(max_length=40, choices=(
        ('CC', 'Cedula Ciudadania'),
        ('CE', 'Cedula Extrangeria'),
        ('PP', 'Pasaporte')
    ))
    DNI_number = models.CharField(max_length=10)
    birthdate = models.DateField(null=True)
    phone_number = models.CharField(max_length=10)
    email = models.CharField(max_length=30, null=True)
    address = models.CharField(max_length=35)
    role = models.CharField(default="US",max_length=15, choices=(
        ('US', 'User'),
        ('ADM', 'Administrator')
    ))
    is_active = models.BooleanField(default=True)
    avatar = models.ImageField(null=True, blank=True)
    id_user_default= models.ForeignKey(
        User,
        related_name="usuariosLoginDefault",
        on_delete=models.SET_NULL,
        null=True
    )
