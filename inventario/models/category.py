from django.db import models

class Category(models.Model):
    id_category = models.AutoField(primary_key=True)
    category_name = models.TextField(null=False)
    category_description = models.TextField(null=True)
    is_active = models.BooleanField(default=True)
