from django.db import models
from django.db.models.deletion import SET_NULL
from django.db.models.fields.related import ForeignKey
from .user import User
from .category import Category

class Product(models.Model):
    id_product = models.AutoField(primary_key=True)
    product_name = models.TextField()
    url_imagen=models.TextField(default='../assets')
    product_image = models.ImageField(upload_to="products",null = True)
    product_description = models.TextField(null=True)
    price = models.FloatField(default=0)
    units_in_stock = models.IntegerField(default=0,null=False)
    id_creator = models.ForeignKey(
        User,   # Tabla a la cual hacecmos referencia
        related_name = "productos_creados",
        on_delete = models.SET_NULL,
        null=True
    )
    
    id_category = models.ForeignKey(
        Category,
        related_name= "categorias",
        on_delete = models.SET_NULL,
        null=True,
        default=1
    )
    