from rest_framework import fields, serializers
from inventario.models.user import User

class AccountSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ['id','DNI_number','DNI_type',
        'name','last_name','email','is_active',
        'birthdate','id_user_default','address','role','avatar','is_active',
        'phone_number']