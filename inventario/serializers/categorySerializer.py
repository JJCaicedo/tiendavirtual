from rest_framework import fields, serializers
from inventario.models.category import Category

class CategorySerializer(serializers.ModelSerializer):
    class Meta:
        model = Category
        fields = ['id_category','category_name','is_active']