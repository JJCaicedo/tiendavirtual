from rest_framework import fields, serializers
from inventario.models.product import Product

class ProductSerializer(serializers.ModelSerializer):
    class Meta:
        model = Product
        fields = ['id_product','product_name','price','id_category','url_imagen','product_image']